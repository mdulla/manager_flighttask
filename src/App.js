import  { BrowserRouter , Routes, Route } from 'react-router-dom';

// import Pagenotfound from './components/pagenotfound';
// import Register from './components/register';
// import Home from './components/homepage/Home';
// import Content from './components/mainpage/maincontent';
// import Login from './components/commonfiles/login';
import Combined from './components/combined';
import Pagenotfound from './components/commonfiles/pagenotfound';
import Register from './components/commonfiles/register';
import Login from './components/commonfiles/login';

function App() {
  return (
    <div>
       
    <BrowserRouter>
      <Routes>
        <Route path ='/' element ={<Combined/>}/>
        
        <Route path = '/register' element = {<Register/>}/>
        <Route path = '/login' element = {<Login/>}/>
      

         <Route path = '*' element = {<Pagenotfound/>} />
         {/* //if the page is not there if show 404 error */}
        {/*<Route path ='/' element ={<Content />}/> */}
      
        {/* <Route path ='/' element ={<Home />}/> */}
         {/* <Route path = '/register' element = {<Register/>}
        <Route path = '/login' element = {<Login/>}/>

         <Route path = '*' element = {<Pagenotfound/>}/>    */}
         {/* //if the page is not there if show 404 error */}
      </Routes> 
      </BrowserRouter> 
  
  
    
   
    </div>
  );
}

export default App;

