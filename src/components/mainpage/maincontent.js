import  React from "react";
import "./mainscript.css";

function Content(){
  return(
  
<div className="bg">
<div className="booking-form-w3layouts">
		{/* <!-- Form starts here --> */}
		<form action="#">
    
    {/* action="#" */}
    <h2 className="sub-heading-agileits">Booking Details</h2>
			<div className="main-flex-w3ls-sectns">
				<div className="field-agileinfo-spc form-w3-agile-text1">
					<select className="form-control">
										<option>From</option>
										<option value="Hyderabad">Hyderabad</option>
										<option value="Adipiscing">Mumbai</option>
                    <option value="Adipiscing">Jaipur International Airport</option>
                    <option value="Adipiscing">Kozhikode Airport, Calicut</option>
                    <option value="Adipiscing">Thiruvananthapuram International Airport</option>
										<option value="Lorem Ipsum">New Delhi</option>
										<option value="Adipiscing">Bangalore International Airport Limited, Bengaluru</option>
										<option value="Lorem Ipsum">Netaji Subhash Chandra Bose International Airport, Kolkata</option>
										<option value="Adipiscing">Chennai International Airport, Chennai</option>
									</select>
				</div>
				<div className="field-agileinfo-spc form-w3-agile-text2">
					<select className="form-control">
										<option>To</option>
										<option value="Lorem Ipsum">Lorem Ipsum</option>
										<option value="Adipiscing">Adipiscing</option>
										<option value="Lorem Ipsum">Lorem Ipsum</option>
										<option value="Adipiscing">Adipiscing</option>
										<option value="Lorem Ipsum">Lorem Ipsum</option>
										<option value="Adipiscing">Adipiscing</option>
									</select>
				</div>
			</div>
			<div className="main-flex-w3ls-sectns">
				<div className="field-agileinfo-spc form-w3-agile-text1">
					<select className="form-control">
										<option>Preferred Airline</option>
										<option value="American Airline">American Airline</option>
										<option value="Delta Airlines">Delta Airlines</option>
										<option value="Frontier Airline">Frontier Airline</option>
										<option value="Jet Blue">Jet Blue</option>
										<option value="Southwest Airlines">Southwest Airlines</option>
									</select>
				</div>
				<div className="field-agileinfo-spc form-w3-agile-text2">
					<select className="form-control">
										<option>Preferred Seating</option>
										<option value="Window">Window</option>
										<option value="Aisle">Aisle</option>
										<option value="Special">Special (Request note below)</option>
									</select>
				</div>
			</div>
			<div className="main-flex-w3ls-sectns">
				<div className="field-agileinfo-spc form-w3-agile-text1">
					<input id="datepicker" name="Text" type="text" placeholder="Departure Date" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'mm/dd/yyyy';}" required="" className="hasDatepicker"/>
				</div>
				<div className="field-agileinfo-spc form-w3-agile-text2">
					<input type="text" id="timepicker" name="Time" className="timepicker form-control hasWickedpicker" placeholder="Departure Time" value="" onkeypress="return false;"/>
				</div>
			</div>

			<div className="triple-wthree">
				<div className="field-agileinfo-spc form-w3-agile-text11">
					<select className="form-control">
												<option value="">Adult(12+ Yrs)</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>         
												<option value="4">4</option>
												<option value="5">5+</option>
											</select>
				</div>
				<div className="field-agileinfo-spc form-w3-agile-text22">
					<select className="form-control">
												<option value="">Children(2-11 Yrs)</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>         
												<option value="4">4</option>
												<option value="5">5+</option>     
											</select>
				</div>
				<div className="field-agileinfo-spc form-w3-agile-text33">
					<select className="form-control">
												<option value="">Infant(under 2Yrs)</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>         
												<option value="4">4</option>
												<option value="5">5+</option>    
											</select>
				</div>
			</div>
			<div className="radio-section">
				<h6>Select your Fare</h6>
				<ul className="radio-buttons-w3-agileits">
					<li>
						<input type="radio" id="a-option" name="selector1"/>
						<label for="a-option">One Way</label>
						<div className="check"></div>
					</li>
					<li>
						<input type="radio" id="b-option" name="selector1"/>
						<label for="b-option">Round-Trip</label>
						<div className="check">
							<div className="inside"></div>
						</div>
					</li>
				</ul>
				<div className="clear"></div>
			</div>
			<div className="main-flex-w3ls-sectns">
				<div className="field-agileinfo-spc form-w3-agile-text1">
        {/* <input type="date" id="datepicker1" name="birthday"/> */}
					<input id="datepicker1" name="Text" type="text" placeholder="Return Date" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'mm/dd/yyyy';}" required="" className="hasDatepicker"/>
				</div>
				<div className="field-agileinfo-spc form-w3-agile-text2">
					<input type="text" id="timepicker1" name="Time" className="timepicker form-control hasWickedpicker" placeholder="Return Time" value="" onkeypress="return false;"/>
				</div>
			</div>
			<div className="field-agileinfo-spc form-w3-agile-text">
				<textarea name="Message" placeholder="Any Message..."></textarea>
			</div>
			{/* <h3 className="sub-heading-agileits">Personal Details</h3>
			<div className="main-flex-w3ls-sectns">
				<div className="field-agileinfo-spc form-w3-agile-text1">
					<input type="text" name="Name" placeholder="Full Name" required=""/>
				</div>
				<div className="field-agileinfo-spc form-w3-agile-text2">
					<input type="text" name="Phone no" placeholder="Phone Number" required=""/>
				</div>
			</div>
			<div className="field-agileinfo-spc form-w3-agile-text">
				<input type="email" name="Email" placeholder="Email" required=""/>
			</div> */}
			<div className="clear"></div>
			<input type="submit" value="Book Flight"/>
			<input type="reset" value="Clear Form"/>
			<div className="clear"></div>

  
 

    </form>
    </div>
    </div>
	

);
}
		// {/* <!--// Form starts here --> */}

 
export default Content;




