// d

// import "./common.css";
// import { useState } from "react";
// import React from "react";
// // import { ReactDOM } from "react";

// const Register = () => {
//   const [values, setValues] = useState({
//     name: '',
//     email: '',
//     password: '',
//     cpassword: ''

//   })
 
//   const [validations, setValidations] = useState({
//     name: '',
//     email: '',
//     password: '',
//     cpassword: ''

//   })
  
//   const validateAll = () => {
//     const regex = /^.*(?=.{5,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/;

//     const { name, email, password, cpassword} = values
//     const validations = { name: '', email: '',password: '', cpassword: '' }
//     let isValid = true
    
//     if (!name) {
//       validations.name = 'Name is required'
//       isValid = false
//     }
    
//     if (name && name.length < 3 || name.length > 50){
//       validations.name = 'Name must contain between 3 and 50 characters'
//       isValid = false
//     }
    
//     if (!email) {
//       validations.email = 'Email is required'
//       isValid = false
//     }
    
//     if ((email && !/\S+@\S+\.\S+/).test(email)) {
//       validations.email = 'Email format must be as example@mail.com'
//       isValid = false
//     }
//     if (!password) {
//       validations.password = "password is required !";
//       isValid = false
//     } 
//     if (!regex.test(password)) {
//       validations.password =
//         "Password must contain atleast one special character, one Upper case, a lower case and a number";
//     }

    
  
    
//     if (!isValid) {
//       setValidations(validations)
//     }
    
//     return isValid
//   }

//   const validateOne = (e) => {
//     const { name } = e.target
//     const value = values[name]
//     let message = ''
    
//     if (!value) {
//       message = `${name} is required`
//     }
    
//     if ((value) && (name === 'name' )&& ((value.length < 3)|| (value.length > 50))) {
//       message = 'Name must contain between 3 and 50 characters'
//     }

//     if ((value && name === 'email') &&(!/\S+@\S+\.\S+/.test(value)) ) {
//       message = 'Email format must be as example@mail.com'
//     }
//     if ((value && name === 'password') &&(!/^.*(?=.{5,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/.test(value)) ) {
//       message ="Password must contain atleast one special character, one Upper case, a lower case and a number"
//     }
//     }
    
//     setValidations({...validations, [name]: message })
//   }
  
//   const handleChange = (e) => {
//     const { name, value } = e.target
//     setValues({...values, [name]: value })
//   }

//   const handleSubmit = (e) => {
//     e.preventDefault()

//     const isValid = validateAll()
    
//     if (!isValid) {
//       return false
//     }

//     // alert(JSON.stringify(values))
//   }
  
//   const { name, email } = values

//   const { 
//     name: nameVal, 
//     email: emailVal, 
//     password: passwordval,
//     cpassword: cpasswordval,
   
//   } = validations
  
//     return (
      
//          {/* <h1>Simple form</h1> */}
//          <div>
//           <div className="registration">  
//           <h1>Registration</h1>
//          <form onSubmit={handleSubmit}>
//           <div>
//             <label>Name:</label>
//               <input 
//                 type="text"
//                 name="name"
//                 id = "name"
//                 value={name} 
//                 onChange={handleChange}
//                 onBlur={validateOne}
//               />
            
//             <div>{nameVal}</div><br/><br/>
//           </div>
          
//           <div>
//             <label>Email:  </label>
//               <input 
//                 type="email"
//                 name="email"
//                 id = "name"
//                 value={email} 
//                 onChange={handleChange}
//                 onBlur={validateOne}
//               />
          
//             <div>{emailVal}</div>
//           </div>
//           <div>
//             <label>Password:  </label>
//               <input 
//                 type="text"
//                 name="password"
//                 id = "name"
//                 value={password} 
//                 onChange={handleChange}
//                 onBlur={validateOne}
//               />
//               <div>{passwordVal}</div>
//               </div>
//               <div>
//               <label>Confirm Password:  </label>
//               <input 
//                 type="text"
//                 name=" cpassword"
//                 id = "name"
//                 value={cpassword} 
//                 onChange={handleChange}
//                 onBlur={validateOne}
//               />
//               <div>{cpasswordval}</div>
//               </div>

           
//           <button type="submit">REGISTER</button>
//           <p>Are you registed already. please login</p>  
     
//               <Link to ='/login'>login</Link>
//           </form>
//     </div>
//     </div>
       
   

      
//     );
// }
// export default Register;

// import { useState, useEffect } from "react";
import React from 'react';
import {Link } from 'react-router-dom';

import { useState } from 'react';
import { useEffect } from 'react';
import "./common.css";
// Validation of the Loginpage.
function Register() {
  const initialvalues = { username: "", password: "" };
  const [formvalues, setFormvalues] = useState(initialvalues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmit, setIsSubmit] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormvalues({ ...formvalues, [name]: value });
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    setFormErrors(validate(formvalues));
    setIsSubmit(true);
  };
  useEffect(() => {
    console.log(formErrors);
    if (Object.keys(formErrors).length === 0 && isSubmit) {
      console.log(formvalues);
    }
  }, [formErrors]);

  const validate = (values) => {
    const errors = {};
    // const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
    const regex = /^.*(?=.{5,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/;

    if (!values.username) {
      errors.username = "username is required !";
    } else if (values.username.length < 6) {
      errors.username = "username must be more than 6 characters";
    }
    if (!values.password) {
      errors.password = "password is required !";
    } else if (!regex.test(values.password)) {
      errors.password =
        "Password must contain atleast one special character, one Upper case, a lower case and a number";
    }

    return errors;
  };

  return (
    <>
      <div className="container">
        {Object.keys(formErrors).length === 0 && isSubmit ? (
          <div className="ui message success">signed in successfully</div>
        ) : (
          <pre>{JSON.stringify(formvalues, undefined, 2)}</pre>
        )}
        <form onSubmit={handleSubmit}>
          <div className="login-form" action="">
            <h1>Register</h1>
            <div className="form-input">
              <input
                type="text"
                name="username"
                id="username"
                placeholder=" name"
                autocomplete="off"
                className="form-control"
                required
                value={formvalues.username}
                onChange={handleChange}
              />
              <label for="username">Username</label>
            </div>
            <p>{formErrors.username}</p>

            <div className="form-input">
              <input
                type="password"
                name="password"
                id="password"
                placeholder="password "
                autocomplete="off"
                className="form-control"
                required
                value={formvalues.password}
                onChange={handleChange}
              />

              <label for="password">Password</label>
            </div>
            <p>{formErrors.password}</p>
            <button type="submit"  className="btn">REGISTER</button>
          <p>Are you registed already. please login</p>  
     
             <div className='ha'><Link to ='/login'>login</Link></div>
             </div>
         
            {/* <button type="submit" className="btn"> */}
             
            
          
        </form>
      </div>
    </>
  );
}
export default Register;

